﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UseItem : MonoBehaviour {

	public GameObject prefab;
	GameObject player;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	public void itemUse()
	{
		if (System.Int32.Parse (this.transform.Find ("Text").GetComponent<Text> ().text) > 1) {
			int count = System.Int32.Parse (this.transform.Find ("Text").GetComponent<Text> ().text) - 1;
			this.transform.Find ("Text").GetComponent<Text> ().text = "" + count;
		} else {
			Destroy (this.gameObject);
		}
	}

	public void dropItem()
	{
		Instantiate (prefab, new Vector3 (player.transform.position.x -1, player.transform.position.y, player.transform.position.z-1), Quaternion.identity);
	}
}
