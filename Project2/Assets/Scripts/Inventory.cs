﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public GameObject inventoryPanel;
	public GameObject[] inventoryItems;

	void OnCollisionEnter(Collision col)
	{
		foreach(Transform child in inventoryPanel.transform)
		{
			if (child.gameObject.tag == col.gameObject.tag) 
			{
				string c = child.Find ("Text").GetComponent<Text> ().text;
				int tcount = System.Int32.Parse (c) + 1;
				child.Find ("Text").GetComponent<Text> ().text = "" + tcount;
				return;
			}
		}

		GameObject i;
		GameObject colObject;

		colObject = col.gameObject;

		if (col.gameObject.tag == "Item1") {
			i = Instantiate (inventoryItems [0]);
			i.transform.SetParent (inventoryPanel.transform);
		} else if (col.gameObject.tag == "Item2") {
			i = Instantiate (inventoryItems [1]);
			i.transform.SetParent (inventoryPanel.transform);
		} else if (col.gameObject.tag == "Item3") {
			i = Instantiate (inventoryItems [2]);
			i.transform.SetParent (inventoryPanel.transform);
		} else if (col.gameObject.tag == "Item4") {
			i = Instantiate (inventoryItems [3]);
			i.transform.SetParent (inventoryPanel.transform);
		} else if (col.gameObject.tag == "Item5") {
			i = Instantiate (inventoryItems [4]);
			i.transform.SetParent (inventoryPanel.transform);
		}else if (col.gameObject.tag == "Item6") {
			i = Instantiate (inventoryItems [5]);
			i.transform.SetParent (inventoryPanel.transform);
		}else if (col.gameObject.tag == "Item7") {
			i = Instantiate (inventoryItems [6]);
			i.transform.SetParent (inventoryPanel.transform);
		}else if (col.gameObject.tag == "Item8") {
			i = Instantiate (inventoryItems [7]);
			i.transform.SetParent (inventoryPanel.transform);
		}else if (col.gameObject.tag == "Item9") {
			i = Instantiate (inventoryItems [8]);
			i.transform.SetParent (inventoryPanel.transform);
		}else if (col.gameObject.tag == "Item10") {
			i = Instantiate (inventoryItems [9]);
			i.transform.SetParent (inventoryPanel.transform);
		}
		if(colObject.name != "Plane")
		Destroy (colObject);
		//Debug.Log(col.gameObject.name); 
	}
		
}
