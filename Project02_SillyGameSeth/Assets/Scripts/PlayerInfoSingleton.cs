﻿using UnityEngine;
using System.Collections;

public class PlayerInfoSingleton : MonoBehaviour {
    public static PlayerInfoSingleton Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
        LoadInformation();
    }
    public void SaveInformation()
    {
        //Save inventory and current level
    }
    public void LoadInformation()
    {
        //Load inventory and current level
    }
}
