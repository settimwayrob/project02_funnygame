﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour
{

    public int currentAttack = 1;
    public bool canAttack;
    public int attackCooldown = 5;
    public bool isAttacking = false;

    public GameObject player;
    public GameObject enemy;
    void Start()
    {
        player = GameObject.Find("Player");
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("Fire1"))
        {
            if (currentAttack == 1)
            {
                //use hoof attack
                isAttacking = true;
            }
            else if (currentAttack == 2)
            {
                //use charge 1
                canAttack = false;
                isAttacking = true;
                attackCooldown--;
                if (attackCooldown == 0)
                {
                    canAttack = true;
                    attackCooldown = 5;
                }
            }
            else if (currentAttack == 3)
            {
                //use charge 2
                canAttack = false;
                attackCooldown--;
                isAttacking = true;
                if (attackCooldown == 0)
                {
                    canAttack = true;
                    attackCooldown = 5;
                }
            }
            else if (currentAttack == 4)
            {
                //use charge 3
                canAttack = false;
                attackCooldown--;
                isAttacking = true;
                if (attackCooldown == 0)
                {
                    canAttack = true;
                    attackCooldown = 5;
                }
            }
            else
                isAttacking = false;
        }
        if (Input.GetKeyDown("One"))
        {
            currentAttack = 1;
            //equip hoof attack
        }
        if (Input.GetKeyDown("Two"))
        {
            currentAttack = 2;
            //equip charge 1
        }
        if (Input.GetKeyDown("Three"))
        {
            currentAttack = 3;
            //equip charge 2
        }
        if (Input.GetKeyDown("Four"))
        {
            currentAttack = 4;
            //equip charge 3
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if(isAttacking == true)
        {
            GameObject.Destroy(enemy);
        }
        else if(isAttacking == false)
        {
            //Remove some of the player's health
        }
    }
}
